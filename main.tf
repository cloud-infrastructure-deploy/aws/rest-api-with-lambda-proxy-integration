# Creation of lambda

resource "aws_iam_role" "fn_proxy_integration_role" {

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        },
        Action = "sts:AssumeRole"
      }
    ]
  })

  description           = "Proxy integration lambda role"
  force_detach_policies = false
  max_session_duration  = 3600
  name                  = "fn-proxy-integration-role"
  path                  = "/"

  tags = {
    Name    = "fn-proxy-integration-role"
    Managed = "Terraform"
  }
}

resource "aws_iam_policy" "fn_proxy_integration_policy" {
  description = "Proxy integration lambda policy"
  name        = "fn-proxy-integration-policy"
  path        = "/"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        Resource = [
          "${aws_cloudwatch_log_group.fn_proxy_integration_lg.arn}:*"
        ]
      }
    ]
  })

  tags = {
    Name    = "fn-proxy-integration-policy"
    Managed = "Terraform"
  }
}

resource "aws_iam_role_policy_attachment" "attach_fn_proxy_integration" {
  role       = aws_iam_role.fn_proxy_integration_role.name
  policy_arn = aws_iam_policy.fn_proxy_integration_policy.arn
}

resource "aws_lambda_function" "fn_proxy_integration" {
  architectures = ["x86_64"]
  description   = "Proxy integration lambda"

  ephemeral_storage {
    size = 512
  }

  filename      = "nodejs.zip"
  function_name = "fn-proxy-integration"
  handler       = "index.handler"
  memory_size   = 512
  role          = aws_iam_role.fn_proxy_integration_role.arn
  runtime       = "nodejs20.x"
  timeout       = 30
}

resource "aws_cloudwatch_log_group" "fn_proxy_integration_lg" {
  log_group_class   = "STANDARD"
  name              = "/aws/lambda/${aws_lambda_function.fn_proxy_integration.function_name}"
  retention_in_days = 1
  skip_destroy      = false
}

resource "aws_api_gateway_rest_api" "rest_api_proxy" {
  name        = "rest-api-proxy"
  description = "Rest api for lambda proxy integration"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "resource_rest_api" {
  rest_api_id = aws_api_gateway_rest_api.rest_api_proxy.id
  parent_id   = aws_api_gateway_rest_api.rest_api_proxy.root_resource_id
  path_part   = "helloworld"
}

resource "aws_api_gateway_method" "method_resource_rest_api" {
  rest_api_id   = aws_api_gateway_rest_api.rest_api_proxy.id
  resource_id   = aws_api_gateway_resource.resource_rest_api.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "ag_fn_proxy_integration" {
  rest_api_id = aws_api_gateway_rest_api.rest_api_proxy.id
  resource_id = aws_api_gateway_resource.resource_rest_api.id
  http_method = aws_api_gateway_method.method_resource_rest_api.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  connection_type         = "INTERNET"
  uri                     = aws_lambda_function.fn_proxy_integration.invoke_arn
  passthrough_behavior    = "WHEN_NO_MATCH" 
  timeout_milliseconds    = 29000
}

resource "aws_lambda_permission" "ag_invoke_fn" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.fn_proxy_integration.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.rest_api_proxy.execution_arn}/*/*"
}

resource "aws_api_gateway_stage" "ag_stage" {
  deployment_id = aws_api_gateway_deployment.ag_deployment.id
  rest_api_id   = aws_api_gateway_rest_api.rest_api_proxy.id
  stage_name    = "prod"
}

resource "aws_api_gateway_deployment" "ag_deployment" {
  rest_api_id = aws_api_gateway_rest_api.rest_api_proxy.id

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.rest_api_proxy.body))
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [ 
    aws_api_gateway_method.method_resource_rest_api, 
    aws_api_gateway_integration.ag_fn_proxy_integration 
  ]
}